import React, { useEffect } from "react";
import { Button, Checkbox, Form, Input, message } from "antd";

import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {
  getDatabase,
  ref,
  push,
  set,
  remove,
  onValue,
  off,
  get,
} from "firebase/database";
import { useState } from "react";
// Optionally import the services that you want to use
// import "firebase/auth";
// import "firebase/database";
// import "firebase/firestore";
// import "firebase/functions";
// import "firebase/storage";
// import { getFirestore } from "firebase/firestore";

export default function HomePage() {
  const [canAdd, setCanAdd] = useState(false); // khởi tạo giá trị ban đầu là false
  const [canDelete, setCanDelete] = useState(false); // xóa giá trị, ban đầu là false
  const [data, setData] = useState([]); //lấy data từ firebase
  console.log("data", data);
  const onFinish = (values) => {
    console.log("Success:", values);
    // Thêm một mục mới vào cơ sở dữ liệu Firebase Realtime
    // Nếu biến state là true thì mới thêm dữ liệu vào Firebase
    if (canDelete) {
      const db = getDatabase();
      const userRef = ref(db, "users/" + values.id);
      remove(userRef)
        .then(() => {
          message.success("Xóa dữ liệu thành công");
        })
        .catch((error) => {
          message.error("Lỗi khi xóa dữ liệu: " + error.message);
        });
    }
    if (canAdd) {
      const db = getDatabase();
      const userRef = ref(db, "users/" + values.id);

      set(userRef, {
        id: values.id,
        email: values.email,
        name: values.name,
      })
        .then(() => {
          message.success("Thêm dữ liệu thành công");
        })
        .catch((error) => {
          message.error("Lỗi khi thêm dữ liệu: ");
        });
    }
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const handleAddClick = () => {
    setCanAdd(true); // cập nhật giá trị của biến state thành true khi nhấn nút "thêm"
    setCanDelete(false);
  };
  const handleDeleteClick = () => {
    setCanAdd(false); // cập nhật lại giá trị của biến state thành false sau khi xóa
    setCanDelete(true);
  };
  const handleEditClick = () => {
    setCanAdd(false); // cập nhật lại giá trị của biến state thành false sau khi xóa
    setCanDelete(false);
  };
  const handleDeleteItem = (id) => {
    const db = getDatabase();
    const userRef = ref(db, "users/" + id);
    remove(userRef)
      .then(() => {
        message.success("Xóa dữ liệu thành công");
      })
      .catch((error) => {
        message.error("Lỗi khi xóa dữ liệu: " + error.message);
      });
  };
  useEffect(() => {
    const firebaseConfig = {
      apiKey: "AIzaSyDY_u2BtZbnttRt8ziePXhup9XM4OudSNU",
      authDomain: "fir-54089.firebaseapp.com",
      databaseURL:
        "https://fir-54089-default-rtdb.asia-southeast1.firebasedatabase.app",
      projectId: "fir-54089",
      storageBucket: "fir-54089.appspot.com",
      messagingSenderId: "898748953688",
      appId: "1:898748953688:web:5f524d338fa4d993efc62d",
      measurementId: "G-9YCPSK46MR",
    };

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const analytics = getAnalytics(app);
  }, []);
  useEffect(() => {
    const db = getDatabase();
    const dataRef = ref(db, "users/");

    // Đăng ký listener để lấy dữ liệu từ Firebase Realtime Database
    onValue(dataRef, (snapshot) => {
      const data = snapshot.val();
      setData(data);
    });

    // Trả về hàm để hủy listener khi component bị unmount
    return () => {
      // Hủy listener khi component bị unmount để tránh memory leak
      off(dataRef);
    };
  }, []);
  // useEffect(() => {
  //   const db = getDatabase();
  //   const dataRef = ref(db, "users/");

  //   // Lấy dữ liệu từ Firebase Realtime Database
  //   get(dataRef).then((snapshot) => {
  //     const data = snapshot.val();
  //     setData(data);
  //   });
  // }, []);
  const renderData = () => {
    const dataArray = Object.values(data); // convert data object to an array
    return dataArray.map((item, index) => {
      return (
        <div className="flex">
          <p key={index}>{item.id}</p>
          <p
            onClick={() => {
              handleDeleteItem(item.id);
            }}
            className="border-2 border-black mb-2 w-1/4 cursor-pointer"
            key={index}
          >
            {item.name}
          </p>
        </div>
      );
    });
  };
  return (
    <div>
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 600,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item label="id" name="id">
          <Input />
        </Form.Item>{" "}
        <Form.Item label="name" name="name">
          <Input />
        </Form.Item>
        <Form.Item label="email" name="email">
          <Input />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button
            onClick={handleAddClick}
            className="bg-blue-500"
            htmlType="submit"
          >
            thêm
          </Button>
          <Button
            onClick={() => handleDeleteClick()}
            className="bg-blue-500"
            htmlType="submit"
          >
            xóa
          </Button>
          <Button
            onClick={handleEditClick}
            className="bg-blue-500"
            htmlType="submit"
          >
            sửa
          </Button>
        </Form.Item>
      </Form>
      {renderData()}
    </div>
  );
}
// if (canAdd) {
// const db = getDatabase();
// push(ref(db, "users"), values)
//   .then(() => {
//     // Thành công, hiển thị thông báo
//     alert("Lưu dữ liệu thành công");
//   })
//   .catch((error) => {
//     // Lỗi, hiển thị thông báo
//     alert(`Lưu dữ liệu thất bại: ${error.message}`);
//   });
// }
